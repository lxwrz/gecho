package main

import (
	"context"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"syscall"
)

func startHttpServer(wg *sync.WaitGroup) *http.Server {
	srv := &http.Server{Addr: ":8042"}

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Printf("curl -X%s \\\n", r.Method)

		fmt.Printf("localhost:8042%s \\\n", r.URL)

		for k, v := range r.Header {
			fmt.Printf("-H \"%s: %s\" \\\n", k, v[0])
		}

		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			fmt.Printf("Failed to read the body: %s\n", err)
			return
		}

		if len(body) > 0 {
			fmt.Printf("--data-raw '%s'\n", body)
		}
	})

	go func() {
		defer wg.Done() // let main know we are done cleaning up

		// always returns error. ErrServerClosed on graceful close
		if err := srv.ListenAndServe(); err != http.ErrServerClosed {
			// unexpected error. port in use?
			log.Fatalf("ListenAndServe(): %v", err)
		}
	}()

	// returning reference so caller can call Shutdown()
	return srv
}

func main() {
	log.Printf("main: starting HTTP server")

	httpServerExitDone := &sync.WaitGroup{}

	httpServerExitDone.Add(1)
	srv := startHttpServer(httpServerExitDone)

	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-c
		// now close the server gracefully ("shutdown")
		// timeout could be given with a proper context
		// (in real world you shouldn't use TODO()).
		log.Printf("main: stopping HTTP server")
		if err := srv.Shutdown(context.TODO()); err != nil {
			panic(err) // failure/timeout shutting down the server gracefully
		}
		os.Exit(1)
	}()

	// wait for goroutine started in startHttpServer() to stop
	httpServerExitDone.Wait()
	log.Printf("main: done. exiting")
}
